import me.oglass.Card;
import me.oglass.Deck;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Objects;

public class TestDeck {
    private final Deck deck = new Deck();

    @BeforeEach
    public void resetDeck() {
        deck.reset();
    }

    @Test
    public void assertAllCards() {
        Card card = deck.dealCard();
        Deck usableDeck = new Deck();
        while (card != null) {
            Card dealtCard = usableDeck.dealCard();
            if (dealtCard == null) throw new RuntimeException("Dealt card was null, but was not supposed to be.");
            assert card.compareTo(dealtCard) == 0;
            card = deck.dealCard();
        }
    }

    @Test
    public void assertAceOfClubs() {
        Card card = deck.getCard(Card.Suit.CLUBS, Card.Value.ACE);
        assert Objects.requireNonNull(card).toString().equals("Ace of Clubs");
    }

    @Test
    public void shuffle() {
        Deck originalDeck = deck.clone();
        deck.shuffle();

        /* Probably will work, 1/2704 chance it won't */
        assert originalDeck.dealCard() != deck.dealCard();
    }
}