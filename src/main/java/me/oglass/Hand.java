package me.oglass;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Hand {
    private final ArrayList<Card> cards = new ArrayList<>();
    public Hand() {}

    public void draw(Card card) {
        for (int i = 0; i < cards.size(); i++) {
            if (cards.get(i).getValue().ordinal() > card.getValue().ordinal()) {
                cards.add(i, card);
                return;
            }
        }
        cards.add(cards.size(), card);
    }

    public Card discard(Card card) {
        return discard(card.getSuit(), card.getValue());
    }

    public Card discard(Card.Suit suit, Card.Value value) {
        return cards
                .stream()
                .filter(card -> card.getSuit() == suit && card.getValue() == value)
                .findFirst()
                .filter(cards::remove) /* What this does is `return cardOptional.isPresent() && cards.remove(cardOptional.get()) ? cardOptional : Optional.empty()` */
                .orElse(null);
    }

    public int compareTo(Hand h) {
        return Math.min(
                Math.max(
                        scoreHand() - h.scoreHand(),
                        -1
                ),
                1
        );
    }

    public int scoreHand() {
        if (isOnePair()) return 1;
        if (isTwoPairs()) return 2;
        if (isThreeOfAKind()) return 3;
        if (isFullHouse()) return 4;
        if (isFourOfAKind()) return 5;
        if (isStraight()) return 6;
        if (isFlush()) return 7;
        if (isRoyalFlush()) return 8;

        /* Return a default */
        return 0;
    }

    public boolean isRoyalFlush() {
        Card.Suit currentSuit = cards.get(0).getSuit();
        var validValues = new ArrayList<>(
                List.of(
                        Card.Value.TEN,
                        Card.Value.JACK,
                        Card.Value.QUEEN,
                        Card.Value.KING,
                        Card.Value.ACE
                )
        );

        return cards
                .stream()
                .limit(5)
                .filter(card -> card.getSuit().equals(currentSuit) && validValues.remove(card.getValue()))
                .count() == 5;
    }

    public boolean isStraightFlush() {
        var firstCard = cards.get(0);
        var lastValue = firstCard.getValue().ordinal();
        for (int i = 1; i < cards.size(); i ++) {
            var card = cards.get(i);
            if (card.getValue().ordinal() - lastValue != 1 || card.getSuit() != firstCard.getSuit()) {
                return false;
            }
            lastValue = card.getValue().ordinal();
        }
        return true;
    }

    public boolean isFourOfAKind() {
        var value = cards.get(2).getValue();
        var suits = Arrays.stream(Card.Suit.values()).toList();

        return cards
                .stream()
                .filter(card -> card.getValue() == value && suits.remove(card.getSuit()))
                .count() == 4;
    }
    public boolean isFullHouse() {
        Card.Value firstValue = cards.get(0).getValue();

        return cards.get(1).getValue() == firstValue &&
                cards.get(2).getValue() == firstValue &&
                cards.get(3).getValue() == cards.get(4).getValue();
    }
    public boolean isFlush() {
        var suit = cards.get(0).getSuit();
        return cards.stream().allMatch(card -> card.getSuit() == suit) && !isStraightFlush();
    }
    public boolean isStraight() {
        Card firstCard = cards.get(0);
        int lastValue = firstCard.getValue().ordinal();
        for (int i = 1; i < cards.size(); i ++) {
            Card card = cards.get(i);
            if (card.getValue().ordinal() - lastValue != 1) {
                return false;
            }
            lastValue = card.getValue().ordinal();
        }
        return true;
    }
    public boolean isThreeOfAKind() {
        Card.Value firstValue = cards.get(0).getValue();

        return cards.get(1).getValue() == firstValue &&
                cards.get(2).getValue() == firstValue &&
                cards.get(3).getValue() != cards.get(4).getValue();
    }
    public boolean isTwoPairs() {
        var pairs = 0;
        for (int i = 0; i < cards.size() - 1; i++) {
            pairs += isPair(i) ? 1 : 0;
        }

        return pairs == 2;
    }
    public boolean isOnePair() {
        var pairs = 0;
        for (int i = 0; i < cards.size() - 1; i++) {
            pairs += isPair(i) ? 1 : 0;
        }

        return pairs == 1;
    }
    private boolean isPair(int start) {
        return cards.get(start).getValue() == cards.get(start + 1).getValue();
    }
    private Card getHighCard() {
        return cards.get(cards.size() - 1);
    }

    @Override
    public String toString() {
        return "Hand{" +
                "cards=" + cards +
                '}';
    }
}