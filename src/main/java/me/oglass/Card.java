package me.oglass;

public class Card implements Comparable<Card> {
    public enum Suit {
        HEARTS("Hearts"),
        CLUBS("Clubs"),
        SPADES("Spades"),
        DIAMONDS("Diamonds");
        private final String displayName;

        Suit(String displayName) {
            this.displayName = displayName;
        }

        public String getDisplayName() {
            return this.displayName;
        }
    }

    public enum Value {
        TWO,
        THREE,
        FOUR,
        FIVE,
        SIX,
        SEVEN,
        EIGHT,
        NINE,
        TEN,
        JACK,
        QUEEN,
        KING,
        ACE
    }

    private final Suit suit;
    private final Value value;

    public Card(Suit suit, Value value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public Value getValue() {
        return value;
    }

    @Override
    public int compareTo(Card card) {
        return value.ordinal() - card.value.ordinal();
    }

    @Override
    public String toString() {
        return this.value.name().substring(0, 1).toUpperCase() +
                this.value.name().substring(1).toLowerCase() +
                " of " +
                this.suit.getDisplayName();
    }
}
