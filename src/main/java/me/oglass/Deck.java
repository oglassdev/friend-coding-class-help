package me.oglass;

import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Objects;
import java.util.Random;

public class Deck implements Cloneable {
    private final ArrayList<Card> cards = new ArrayList<>();

    public Deck() {
        createCards();
    }

    @Nullable
    public Card dealCard() {
        return cards.isEmpty() ? null : cards.remove(0);
    }

    /**
     * Puts a card in the deck.
     *
     * @throws IllegalStateException If the position is not 0, 1, or 2
     *
     * @param card The card to place in the deck
     * @param position 0 for top, 1 for bottom, 2 for random
     */
    public void putCard(Card card, int position) {
        switch(position) {
            /* Top */
            case 0 -> cards.add(card);
            /* Bottom */
            case 1 -> cards.add(0, card);
            /* Random */
            case 2 -> cards.add(new Random().nextInt(0, cards.size()), card);
            default -> throw new IllegalStateException("Unexpected value: " + position);
        }
    }

    @Nullable
    public Card getCard(Card.Suit suit, Card.Value value) {
        return cards
                .stream()
                .filter(card -> card.getSuit().equals(suit) && card.getValue().equals(value))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    public Card getCard(Card card) {
        return cards.contains(card) ? card : null;
    }

    public void reset() {
        cards.clear();
        createCards();
    }

    public void shuffle() {
        Collections.shuffle(cards);
    }

    private void createCards() {
        for (Card.Suit suit : Card.Suit.values()) {
            for (Card.Value value : Card.Value.values()) {
                cards.add(new Card(suit, value));
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Deck deck = (Deck) o;
        return Objects.equals(cards, deck.cards);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cards);
    }

    @Override
    public Deck clone() {
        Deck deck = new Deck();
        deck.cards.clear();
        cards.forEach((card) -> deck.putCard(card, 0));
        return deck;
    }
}
